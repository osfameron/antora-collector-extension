'use strict'

const { promises: fsp } = require('fs')
const { spawnSync } = require('child_process')

;(async () => {
  const tag = spawnSync('git', ['describe', '--tags']).stdout.toString().trimRight()
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  await fsp.writeFile('build/antora.yml', `name: test\nversion: '${tag.substr(1)}'\n`, 'utf8')
  await fsp.writeFile(`build/modules/ROOT/pages/${tag}-release.adoc`, '= Tag-Specific Page', 'utf8')
})()
