'use strict'

const { promises: fsp } = require('fs')

;(async () => {
  await fsp.mkdir('build/modules/more/pages', { recursive: true })
  await fsp.writeFile('build/modules/more/pages/index.adoc', '= Another Page', 'utf8')
})()
