'use strict'

const { promises: fsp } = require('fs')

;(async () => {
  await fsp.mkdir('build', { recursive: true })
  const antoraYml = 'name: test\nversion: 1.0.0\n'
  await fsp.writeFile('build/antora.yml', antoraYml, 'utf8')
})()
