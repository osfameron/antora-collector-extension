/* eslint-env mocha */
'use strict'

const {
  captureStderr,
  captureStdout,
  closeServer,
  expect,
  heredoc,
  startGitServer,
  trapAsyncError,
  updateYamlFile,
  windows,
} = require('@antora/collector-test-harness')
const aggregateContent = require('@antora/content-aggregator')
const { createHash } = require('crypto')
const fs = require('fs')
const { promises: fsp } = fs
const { isAsyncFunction: isAsync } = require('util').types
const getUserCacheDir = require('cache-directory')
const git = require('@antora/content-aggregator/git')
const ospath = require('path')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const WORK_DIR = ospath.join(__dirname, 'work')
const CACHE_DIR = ospath.join(WORK_DIR, 'cache')
const REPOS_DIR = ospath.join(WORK_DIR, 'repos')

describe('collector extension', () => {
  let gitServer
  let gitServerPort

  const cleanWorkDir = async (opts = {}) => {
    await fsp.rm(WORK_DIR, { recursive: true, force: true })
    if (opts.create) await fsp.mkdir(WORK_DIR, { recursive: true })
  }

  const getCollectorCacheDir = () => {
    return ospath.join(CACHE_DIR, 'collector')
  }

  const getCollectorWorktree = (origin) => {
    let folderName
    if (origin.worktree === undefined) {
      folderName = ospath.basename(origin.gitdir, '.git')
    } else {
      const url = origin.url
      folderName = `${url.substr(url.lastIndexOf('/') + 1)}-${createHash('sha1').update(url).digest('hex')}`
    }
    return ospath.join(getCollectorCacheDir(), folderName)
  }

  before(async () => {
    await cleanWorkDir({ create: true })
    ;[gitServer, gitServerPort] = await startGitServer(REPOS_DIR)
  })

  after(async () => {
    await closeServer(gitServer.server)
    await fsp.rm(WORK_DIR, { recursive: true, force: true })
  })

  afterEach(async () => {
    await cleanWorkDir()
  })

  describe('bootstrap', () => {
    it('should be able to require extension', () => {
      const exports = require('@antora/collector-extension')
      expect(exports).to.be.instanceOf(Object)
      expect(exports.register).to.be.instanceOf(Function)
    })
  })

  describe('integration', () => {
    const ext = require('@antora/collector-extension')

    const createGeneratorContext = () => ({
      require: () => git,
      once (eventName, fn) {
        this[eventName] = fn
      },
    })

    const createRepository = async ({ repoName, fixture = repoName, branches, tags, startPath, collectorConfig }) => {
      const repo = { dir: ospath.join(REPOS_DIR, repoName), fs }
      const links = []
      const captureLinks = function (src, dest) {
        if (!src.endsWith('-link')) return true
        return fsp.readFile(src, 'utf8').then((link) => {
          const [from, to] = link.trim().split(' -> ')
          this.push([ospath.join(ospath.dirname(dest), from), to])
          return false
        })
      }
      await fsp.cp(ospath.join(FIXTURES_DIR, fixture), repo.dir, { recursive: true, filter: captureLinks.bind(links) })
      for (const [from, to] of links) {
        const type = to.endsWith('/') ? 'dir' : 'file'
        await fsp.symlink(type === 'dir' ? to.slice(0, -1) : to, from, type)
      }
      if (collectorConfig) {
        const antoraYmlPath = ospath.join(repo.dir, startPath || '', 'antora.yml')
        await updateYamlFile(antoraYmlPath, { ext: { collector: collectorConfig } })
      }
      await git.init({ ...repo, defaultBranch: 'main' })
      await git
        .statusMatrix(repo)
        .then((status) =>
          Promise.all(
            status.map(([filepath, _, worktreeStatus]) =>
              worktreeStatus === 0 ? git.remove({ ...repo, filepath }) : git.add({ ...repo, filepath })
            )
          )
        )
      await git.commit({ ...repo, author: { name: 'Tester', email: 'tester@example.org' }, message: 'initial import' })
      if (branches) {
        for (const branch of branches) await git.branch({ ...repo, ref: branch })
      }
      if (tags) {
        for (const tag of tags) await git.tag({ ...repo, ref: tag })
      }
      repo.url = `http://localhost:${gitServerPort}/${repoName}/.git`
      return repo
    }

    const runScenario = async ({ repoName, branches, tags, startPath, local, collectorConfig, before, after }) => {
      const repo = await createRepository({ repoName, branches, tags, startPath, collectorConfig })
      const playbook = {
        runtime: { cacheDir: CACHE_DIR, quiet: true },
        content: {
          sources: [
            {
              url: local ? repo.dir : repo.url,
              startPath,
              branches: branches || 'main',
              tags,
            },
          ],
        },
      }
      const contentAggregate = await aggregateContent(playbook)
      if (before) isAsync(before) ? await before(contentAggregate, playbook) : before(contentAggregate, playbook)
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      await generatorContext.contentAggregated({ playbook, contentAggregate })
      if (after) isAsync(after) ? await after(contentAggregate) : after(contentAggregate)
    }

    it('should not allocate worktree for reference in git tree if collector is not configured', async () => {
      await runScenario({
        repoName: 'test-at-root',
        before: async (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.files).to.be.empty()
          const worktree = getCollectorWorktree(bucket.origins[0])
          await fsp.mkdir(ospath.dirname(worktree), { recursive: true })
          await fsp.writeFile(worktree, Buffer.alloc(0))
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.be.empty()
        },
      })
    })

    it('should run specified command in temporary worktree and collect files in scan dir', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should populate properties of file collected from temporary worktree', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          const bucket = contentAggregate[0]
          const files = bucket.files
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const expectedRealpath = ospath.join(getCollectorWorktree(bucket.origins[0]), expectedScanned)
          expect(files).to.have.lengthOf(1)
          expect(files[0]).to.have.property('stat')
          expect(files[0].src).to.eql({
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            realpath: expectedRealpath,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          })
        },
      })
    })

    it('should populate properties of file collected from start path in temporary worktree', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-docs-start-page.js' },
        scan: { dir: 'build/docs' },
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        after: (contentAggregate) => {
          const bucket = contentAggregate[0]
          const files = bucket.files
          const expectedScanned = 'build/docs/modules/ROOT/pages/index.adoc'
          const expectedRealpath = ospath.join(getCollectorWorktree(bucket.origins[0]), expectedScanned)
          expect(files).to.have.lengthOf(1)
          expect(files[0].path).to.equal('modules/ROOT/pages/index.adoc')
          expect(files[0].src).to.eql({
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            realpath: expectedRealpath,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          })
        },
      })
    })

    it('should populate properties of file collected from concrete worktree', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const expectedAbspath = ospath.join(contentAggregate[0].origins[0].worktree, expectedScanned)
          expect(contentAggregate[0].files[0]).to.have.property('stat')
          expect(contentAggregate[0].files[0].src).to.eql({
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedAbspath,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          })
        },
      })
    })

    it('should update component metadata from antora.yml file, if found', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-component-desc.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('main')
          expect(bucket.files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('1.0.0')
          expect(bucket.title).to.equal('Test')
          expect(bucket.files).to.be.empty()
          expect(bucket).to.have.nested.property('asciidoc.attributes.url-api', 'https://api.example.org')
          const expectedScriptDirname = getCollectorWorktree(bucket.origins[0])
          expect(bucket.asciidoc.attributes['script-dirname']).to.equal(expectedScriptDirname)
        },
      })
    })

    it('should remove prerelease key if not specified in scanned antora.yml file', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-component-desc.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root-prerelease',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('')
          expect(bucket.prerelease).to.be.true()
          expect(bucket.files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('1.0.0')
          expect(bucket.prerelease).to.be.undefined()
          expect(bucket.files).to.be.empty()
        },
      })
    })

    it('should only update contents of existing page', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-replace-start-page.js' },
        scan: { dir: 'build' },
      }
      let originalStat
      await runScenario({
        repoName: 'test-replace',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].contents.toString()).to.include('= Stub Start Page')
          originalStat = contentAggregate[0].files[0].stat
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          const expectedContents = heredoc`
            = Real Start Page

            This is the real deal.
          `
          expect(contentAggregate[0].files[0].contents.toString()).to.equal(expectedContents + '\n')
          expect(contentAggregate[0].files[0].stat).to.not.equal(originalStat)
          expect(contentAggregate[0].files[0].stat.size).to.not.equal(originalStat.size)
          expect(contentAggregate[0].files[0].src).to.not.have.property('scanned')
        },
      })
    })

    it('should run specified command in start path of temporary worktree and collect files in scan dir', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js', dir: '.' },
        scan: { dir: './build' },
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified command from root of temporary worktree and collect files in scan dir', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-docs-start-page.js' },
        scan: { dir: 'build/docs' },
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should prepend node path to command identified as a JavaScript file', async () => {
      const collectorConfig = {
        run: { command: '.gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified local executable command if command begins with ./', async () => {
      const collectorConfig = {
        run: { command: './.gen-start-page' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified local executable command if local option is true', async () => {
      const collectorConfig = {
        run: { command: '.gen-start-page', local: true },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified command from content root if dir option is .', async () => {
      const collectorConfig = {
        run: { command: '.gen-start-page', local: true, dir: '.' },
        scan: { dir: './build' },
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should not collect files if scan key is not specified', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.be.empty()
        },
      })
    })

    it('should not collect files if scan dir key is not a string', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: {},
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.be.empty()
        },
      })
    })

    it('should collect files without running command if only scan key is specified', async () => {
      const collectorConfig = {
        scan: { dir: 'other-docs' },
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/ROOT/pages/outside-start-path.adoc')
        },
      })
    })

    it('should rebase files if base key is set on scan', async () => {
      const collectorConfig = {
        scan: { dir: 'code', base: 'modules/extend/examples' },
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/extend/examples/extended-pdf-converter.rb')
        },
      })
    })

    it('should drop leading / on value of base key', async () => {
      const collectorConfig = {
        scan: { dir: 'code', base: '/modules/extend/examples' },
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].path).to.equal('modules/extend/examples/extended-pdf-converter.rb')
        },
      })
    })

    it('should collect files from multiple scan dirs', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js', dir: '.' },
        scan: [{ dir: './build' }, { dir: 'other-docs' }],
      }
      await runScenario({
        repoName: 'test-at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(2)
          expect(contentAggregate[0].files.map((it) => it.path)).to.have.members([
            'modules/ROOT/pages/index.adoc',
            'modules/ROOT/pages/outside-start-path.adoc',
          ])
        },
      })
    })

    it('should process each collector entry', async () => {
      const collectorConfig = [
        {
          run: { command: 'node .gen-files.js' },
          scan: { dir: 'build' },
        },
        {
          run: { command: 'node .gen-component-desc.js' },
          scan: { dir: 'build', files: 'antora.yml' },
        },
      ]
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].version).to.eql('main')
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('1.0.0')
          expect(bucket.files).to.have.lengthOf(3)
          expect(bucket.files.map((it) => it.path)).to.have.members([
            'modules/ROOT/pages/index.adoc',
            'modules/ROOT/partials/summary.adoc',
            'modules/ROOT/examples/ack.rb',
          ])
          expect(
            contentAggregate[0].files.find((it) => it.path === 'modules/ROOT/pages/index.adoc').contents.toString()
          ).to.include('= The Start Page')
        },
      })
    })

    it('should not clean worktree between collector entries', async () => {
      const collectorConfig = [
        {
          run: { command: 'node .gen-files.js' },
        },
        {
          run: { command: 'node .gen-component-desc.js' },
        },
        {
          run: { command: 'node .gen-start-page.js' },
          scan: [
            { dir: 'build', files: 'antora.yml' },
            { dir: 'build', files: 'modules/**/*' },
          ],
        },
      ]
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].version).to.eql('main')
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('1.0.0')
          expect(bucket.files).to.have.lengthOf(3)
          expect(bucket.files.map((it) => it.path)).to.have.members([
            'modules/ROOT/pages/index.adoc',
            'modules/ROOT/partials/summary.adoc',
            'modules/ROOT/examples/ack.rb',
          ])
          expect(
            contentAggregate[0].files.find((it) => it.path === 'modules/ROOT/pages/index.adoc').contents.toString()
          ).to.include('= Start Page')
        },
      })
    })

    it('should only collect files that match the scan pattern', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-files.js' },
        scan: { dir: 'build', files: '**/*.adoc' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(2)
          const paths = contentAggregate[0].files.map((it) => it.path)
          expect(paths).to.have.members(['modules/ROOT/pages/index.adoc', 'modules/ROOT/partials/summary.adoc'])
        },
      })
    })

    it('should support an array of scan patterns with inclusions and exclusions', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-files.js' },
        scan: { dir: 'build', files: ['**/*.adoc', '!modules/*/partials/**/*.adoc', 'modules/**/*.rb'] },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(2)
          const paths = contentAggregate[0].files.map((it) => it.path)
          expect(paths).to.have.members(['modules/ROOT/pages/index.adoc', 'modules/ROOT/examples/ack.rb'])
        },
      })
    })

    it('should create dedicated cache folder for collector under Antora cache dir', async () => {
      await fsp.mkdir(CACHE_DIR, { recursive: true })
      await fsp.writeFile(getCollectorCacheDir(), Buffer.alloc(0))
      expect(await trapAsyncError(() => runScenario({ repoName: 'test-at-root' }))).to.throw('file already exists')
    })

    it('should create cache folder under user cache if cache dir is not specified', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        before: (_, playbook) => {
          delete playbook.runtime.cacheDir
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          expect(contentAggregate[0].files[0].src.realpath.startsWith(getUserCacheDir('antora-test'))).to.be.true()
        },
      })
    })

    it('should remove dedicated cache dir for collector after run', async () => {
      await runScenario({ repoName: 'test-at-root' })
      expect(getCollectorCacheDir()).to.not.be.a.path()
    })

    it('should reuse worktree if available', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        local: true,
        collectorConfig,
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.have.lengthOf(1)
          const worktree = contentAggregate[0].origins[0].worktree
          expect(ospath.join(worktree, 'build')).to.be.a.directory()
          expect(ospath.join(worktree, 'build/modules/ROOT/pages/index.adoc')).to.be.a.file()
        },
      })
    })

    it('should clean scan dir in worktree before running command(s)', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        local: true,
        collectorConfig,
        before: async (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].files).to.be.empty()
          const worktree = contentAggregate[0].origins[0].worktree
          const residualPagePath = ospath.join(worktree, 'build/modules/ROOT/pages/residual-page.adoc')
          await fsp.mkdir(ospath.dirname(residualPagePath), { recursive: true })
          await fsp.writeFile(residualPagePath, '= Residual Page', 'utf8')
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files.map((it) => it.path)).to.not.include('modules/ROOT/pages/residual-page.adoc')
        },
      })
    })

    it('should run specified command in temporary worktree if repository is local and reference is not worktree', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        local: true,
        branches: ['other'],
        after: (contentAggregate) => {
          const bucket = contentAggregate[0]
          const expectedRealpath = ospath.join(
            getCollectorWorktree(bucket.origins[0]),
            'build/modules/ROOT/pages/index.adoc'
          )
          expect(bucket.files).to.have.lengthOf(1)
          expect(bucket.files[0].src.realpath).to.equal(expectedRealpath)
        },
      })
    })

    it('should run specified command in each branch that defines collector config', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        local: true,
        branches: ['v1.0.x', 'v2.0.x'],
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
          contentAggregate.forEach((bucket) => {
            expect(bucket.origins[0].refname).to.be.oneOf(['v1.0.x', 'v2.0.x'])
            const expectedRealpath = ospath.join(
              getCollectorWorktree(bucket.origins[0]),
              'build/modules/ROOT/pages/index.adoc'
            )
            expect(bucket.files).to.have.lengthOf(1)
            expect(bucket.files[0].src.realpath).to.equal(expectedRealpath)
          })
        },
      })
    })

    it('should run specified command in branch that defines collector config when branch name contains segments', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-component-desc-for-ref.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        branches: ['docs/release-line/1.0'],
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].version).to.equal('docs-release-line-1.0')
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('1.0')
          expect(bucket.origins[0].refname).to.equal('docs/release-line/1.0')
          expect(bucket.files).to.be.empty()
        },
      })
    })

    it('should run specified command in tag that defines collector config', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-for-tag.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        branches: [],
        tags: ['v1.0.1'],
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(contentAggregate[0].origins[0].refname).to.equal('v1.0.1')
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.version).to.equal('1.0.1')
          const expectedRealpath = ospath.join(
            getCollectorWorktree(bucket.origins[0]),
            'build/modules/ROOT/pages/v1.0.1-release.adoc'
          )
          expect(bucket.files).to.have.lengthOf(1)
          expect(bucket.files[0].src.realpath).to.equal(expectedRealpath)
        },
      })
    })

    it('should run command inside worktree for both branches and tags in same repository', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-component-desc-for-ref.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        branches: ['v1.0.x'],
        tags: ['v1.0.1'],
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
          expect(contentAggregate.map((it) => it.origins[0].refname)).to.have.members(['v1.0.1', 'v1.0.x'])
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
          const branchBucket = contentAggregate.find((it) => it.origins[0].reftype === 'branch')
          expect(branchBucket.version).to.equal('1.0')
          expect(branchBucket.prerelease).to.be.true()
          const tagBucket = contentAggregate.find((it) => it.origins[0].reftype === 'tag')
          expect(tagBucket.version).to.equal('1.0.1')
          expect(tagBucket.prerelease).to.be.false()
        },
      })
    })

    it('should not modify index of local repository when checking out ref to worktree', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      let expectedIndex
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        local: true,
        branches: ['v1.0.x'],
        before: async (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expectedIndex = await fsp.readFile(ospath.join(contentAggregate[0].origins[0].gitdir, 'index'), 'utf8')
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          expect(ospath.join(contentAggregate[0].origins[0].gitdir, 'index')).to.have.contents(expectedIndex)
        },
      })
    })

    it('should remove invalid temporary worktree left behind by previous run', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-component-desc.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        branches: ['v1.0.x'],
        before: async (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket).to.have.property('version', 'v1.0.x')
          expect(bucket.files).to.be.empty()
          const worktree = getCollectorWorktree(bucket.origins[0])
          await fsp.mkdir(worktree, { recursive: true })
          await fsp.writeFile(ospath.join(worktree, '.git'), 'nuke me')
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket).to.have.property('version', '1.0.0')
          expect(bucket.files).to.be.empty()
        },
      })
    })

    it('should not leave behind index file in managed repository', async () => {
      const collectorConfig = {
        run: { command: 'node .check-index-file.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        branches: ['v1.0.x'],
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          const bucket = contentAggregate[0]
          expect(bucket.files).to.have.lengthOf(1)
          expect(ospath.basename(bucket.files[0].path)).to.equal('does-not-exist.adoc')
        },
      })
    })

    it('should remove untracked changes when switching worktrees for same repository', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-dirty-worktree.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        branches: ['v1.0.x', 'v2.0.x'],
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
          const [bucket1, bucket2] = contentAggregate
          expect(bucket1).to.have.property('version', 'v1.0.x')
          expect(bucket2).to.have.property('version', 'v2.0.x')
        },
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
          const [bucket1, bucket2] = contentAggregate
          expect(bucket1).to.have.property('version', '1.0')
          expect(bucket1.files).to.have.lengthOf(1)
          expect(bucket1.files[0].path).to.equal('modules/ROOT/pages/v1.0.x-release.adoc')
          expect(bucket2).to.have.property('version', '2.0')
          expect(bucket2.files).to.have.lengthOf(1)
          expect(bucket2.files[0].path).to.equal('modules/ROOT/pages/v2.0.x-release.adoc')
        },
      })
    })

    it('should not follow symlinks when removing untracked files in worktree', async () => {
      const collectorConfig = {
        run: { command: 'node .check-dirty-worktree.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-with-symlink',
        collectorConfig,
        branches: ['v1.0.x', 'v2.0.x'],
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
          const [bucket1, bucket2] = contentAggregate
          expect(bucket1.files).to.have.lengthOf(3)
          expect(bucket1.files.filter((it) => it.path.endsWith('/Hello.java'))).to.have.lengthOf(2)
          expect(bucket1.files.filter((it) => it.path.endsWith('/dirty-worktree.adoc'))).to.be.empty()
          expect(bucket2.files).to.have.lengthOf(3)
          expect(bucket2.files.filter((it) => it.path.endsWith('/Hello.java'))).to.have.lengthOf(2)
          expect(bucket2.files.filter((it) => it.path.endsWith('/dirty-worktree.adoc'))).to.be.empty()
        },
      })
    })

    it('should force checkout if worktree has changes when switching worktrees for same repository', async () => {
      const collectorConfig = {
        run: { command: 'node .gen-dirty-worktree.js delete' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'test-at-root',
        collectorConfig,
        branches: ['v1.0.x', 'v2.0.x'],
        after: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(2)
          const [bucket1, bucket2] = contentAggregate
          expect(bucket1).to.have.property('version', '1.0')
          expect(bucket1.files).to.have.lengthOf(1)
          expect(bucket1.files[0].path).to.equal('modules/ROOT/pages/v1.0.x-release.adoc')
          expect(bucket2).to.have.property('version', '2.0')
          expect(bucket2.files).to.have.lengthOf(1)
          expect(bucket2.files[0].path).to.equal('modules/ROOT/pages/v2.0.x-release.adoc')
        },
      })
    })

    it('should send output from command to stdout by default', async () => {
      const collectorConfig = { run: { command: 'node .gen-output.js' } }
      const stdout = await captureStdout(() =>
        runScenario({
          repoName: 'test-at-root',
          collectorConfig,
          before: (_, playbook) => {
            delete playbook.runtime.quiet
          },
        })
      )
      expect(stdout).to.equal('start\nall done!\n')
    })

    // TODO: perhaps direct these messages to the log?
    it('should always direct stderr output of command to stderr', async () => {
      const collectorConfig = { run: { command: 'node .gen-error-output.js' } }
      const stderr = await captureStderr(() => runScenario({ repoName: 'test-at-root', collectorConfig }))
      expect(stderr).to.equal('there were some issues\n')
    })

    if (!windows) {
      // FIXME reenable on Windows
      it('should escape quoted string inside quoted string in command', async () => {
        const collectorConfig = { run: { command: 'node -p \'"start\\nall done!"\'' } }
        const stdout = await captureStdout(() =>
          runScenario({
            repoName: 'test-at-root',
            collectorConfig,
            before: (_, playbook) => {
              delete playbook.runtime.quiet
            },
          })
        )
        expect(stdout).to.equal('start\nall done!\n')
      })
    }

    it('should not send output from command to stdout by default if quiet is set', async () => {
      const collectorConfig = { run: { command: 'node .gen-output.js' } }
      const stdout = await captureStdout(() => runScenario({ repoName: 'test-at-root', collectorConfig }))
      expect(stdout).to.be.empty()
    })

    it('should throw error if command is not found', async () => {
      const collectorConfig = { run: { command: 'no-such-command' } }
      const expectedMessage = windows
        ? 'Command failed: '
        : '(@antora/collector-extension): Command not found: no-such-command in ' +
          `http://localhost:${gitServerPort}/test-at-root/.git (branch: main)`
      expect(await trapAsyncError(() => runScenario({ repoName: 'test-at-root', collectorConfig }))).to.throw(
        Error,
        expectedMessage
      )
    })

    it('should throw error if command is not found for origin with start path', async () => {
      const collectorConfig = { run: { command: 'no-such-command' } }
      const expectedMessage = windows
        ? 'Command failed: '
        : '(@antora/collector-extension): Command not found: no-such-command in ' +
          `http://localhost:${gitServerPort}/test-at-start-path/.git (branch: main | start path: docs)`
      expect(
        await trapAsyncError(() => runScenario({ repoName: 'test-at-start-path', collectorConfig, startPath: 'docs' }))
      ).to.throw(Error, expectedMessage)
    })

    it('should throw error if command is not found when using local worktree', async () => {
      const collectorConfig = { run: { command: 'no-such-command' } }
      const expectedMessage = windows
        ? 'Command failed: '
        : '(@antora/collector-extension): Command not found: no-such-command in ' +
          `${ospath.join(REPOS_DIR, 'test-at-root')} (branch: main <worktree>)`
      expect(
        await trapAsyncError(() => runScenario({ repoName: 'test-at-root', collectorConfig, local: true }))
      ).to.throw(Error, expectedMessage)
    })

    it('should not run collector if no origins are found on content aggregate', async () => {
      await runScenario({
        repoName: 'test-at-root',
        before: (contentAggregate) => {
          expect(contentAggregate).to.have.lengthOf(1)
          delete contentAggregate[0].origins
        },
        after: (contentAggregate) => {
          expect(contentAggregate[0].files).to.be.empty()
        },
      })
    })

    it('should run collector per origin on which it is defined', async () => {
      const repo1 = await createRepository({
        repoName: 'test-at-root-1',
        fixture: 'test-at-root',
        collectorConfig: {
          run: { command: 'node .gen-start-page.js' },
          scan: { dir: 'build' },
        },
      })
      const repo2 = await createRepository({ repoName: 'test-at-root-2', fixture: 'test-at-root' })
      const repo3 = await createRepository({
        repoName: 'test-at-root-3',
        fixture: 'test-at-root',
        collectorConfig: {
          run: { command: 'node .gen-more-files.js' },
          scan: { dir: 'build' },
        },
      })
      const playbook = {
        runtime: { cacheDir: CACHE_DIR, quiet: true },
        content: {
          sources: [
            { url: repo1.url, branches: 'main' },
            { url: repo2.url, branches: 'main' },
            { url: repo3.url, branches: 'main' },
          ],
        },
      }
      const contentAggregate = await aggregateContent(playbook)
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext)
      await generatorContext.contentAggregated({ playbook, contentAggregate })
      expect(contentAggregate).to.have.lengthOf(1)
      expect(contentAggregate[0].files.map((it) => it.path)).to.have.members([
        'modules/ROOT/pages/index.adoc',
        'modules/more/pages/index.adoc',
      ])
    })
  })
})
