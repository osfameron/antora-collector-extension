'use strict'

const { createHash } = require('crypto')
const expandPath = require('@antora/expand-path-helper')
const forEach = (write) => new Writable({ objectMode: true, write })
const fs = require('fs')
const { promises: fsp } = fs
const getUserCacheDir = require('cache-directory')
const git = require('isomorphic-git')
const globStream = require('glob-stream')
const invariably = { false: () => false, null: () => null, void: () => undefined }
const ospath = require('path')
const { posix: path } = ospath
const posixify = ospath.sep === '\\' ? (p) => p.replace(/\\/g, '/') : undefined
const runCommand = require('./util/run-command.js')
const { pipeline, Writable } = require('stream')
const yaml = require('js-yaml')

const GLOB_OPTS = { allowEmpty: true, follow: true, nomount: true, nosort: true, nounique: true, strict: false }

module.exports.register = function () {
  this.once('contentAggregated', async ({ playbook, contentAggregate }) => {
    const quiet = playbook.runtime?.quiet
    const cacheDir = ospath.join(getBaseCacheDir(playbook), 'collector')
    //await fsp.rm(cacheDir, { force: true, recursive: true, force: true }) // Q: should we try to reuse existing cache?
    await fsp.mkdir(cacheDir, { recursive: true })
    const gitCache = {}
    for (const componentVersionBucket of contentAggregate) {
      const { files: filesInBucket, origins = [] } = componentVersionBucket
      for (const origin of origins) {
        const { url, gitdir, refname, reftype, remote, worktree, startPath, descriptor } = origin
        const collectorConfig = descriptor?.ext?.collector || []
        if (Array.isArray(collectorConfig) && !collectorConfig.length) continue
        const worktreeDir = worktree || ospath.join(cacheDir, generateWorktreeFolderName({ url, gitdir, worktree }))
        const expandPathContext = { base: worktreeDir, cwd: worktreeDir, dot: ospath.join(worktreeDir, startPath) }
        const scanDirs = new Set()
        const collectors = (Array.isArray(collectorConfig) ? collectorConfig : [collectorConfig]).map((collector) => {
          const { run: runConfig = {}, scan: scanConfig = [] } = collector
          return {
            run: {
              ...runConfig,
              cwd: typeof runConfig.dir === 'string' ? expandPath(runConfig.dir, expandPathContext) : worktreeDir,
            },
            scan: (Array.isArray(scanConfig) ? scanConfig : [scanConfig]).reduce((accum, scan) => {
              if (typeof scan.dir === 'string') {
                const dir = expandPath(scan.dir, expandPathContext)
                scanDirs.add(dir)
                accum.push({ ...scan, dir })
              }
              return accum
            }, []),
          }
        })
        if (worktree) {
          for (const scanDir of scanDirs) await fsp.rm(scanDir, { recursive: true, force: true })
        } else {
          const cache = gitCache[gitdir] || (gitCache[gitdir] = {})
          const ref = `refs/${reftype === 'branch' ? 'head' : reftype}s/${refname}`
          // Q: if repo is local, should we operate on a clone instead?
          await prepareWorktree({ fs, cache, dir: worktreeDir, gitdir, ref, remote, bare: worktree === undefined })
        }
        for (const { run, scan: scans } of collectors) {
          const { cwd, command, local } = run
          if (command) {
            let cmd = command
            const opts = { cwd, output: true, quiet }
            if (local) {
              opts.local = true
            } else if (cmd.startsWith('./')) {
              cmd = cmd.slice(2)
              opts.local = true
            } else if (cmd.startsWith('node ') ? (cmd = cmd.slice(5)) : cmd.split(' ', 2)[0].endsWith('.js')) {
              cmd = `"${process.execPath}" ${cmd}`
            }
            try {
              await runCommand(cmd, [], opts)
            } catch (err) {
              const loc = worktree || url
              const flag = worktree ? ' <worktree>' : remote && worktree === false ? ` <remotes/${remote}>` : ''
              const pathInfo = startPath ? ` | start path: ${startPath}` : ''
              const ctx = ` in ${loc} (${reftype}: ${refname}${flag}${pathInfo})`
              throw Object.assign(err, { message: `(@antora/collector-extension): ${err.message.replace(/$/m, ctx)}` })
            }
          }
          for (const scan of scans) {
            for (const file of await srcFs(scan.dir, scan.files, scan.base)) {
              let existingFile
              const relpath = file.path
              if (relpath === 'antora.yml') {
                const generated = yaml.load(file.contents)
                Object.assign(componentVersionBucket, generated)
                if (!('prerelease' in generated)) delete componentVersionBucket.prerelease
              } else if ((existingFile = filesInBucket.find(({ path: path_ }) => path_ === relpath))) {
                Object.assign(existingFile, { contents: file.contents, stat: file.stat })
              } else {
                const src = file.src
                const scannedRelpath = src.abspath.substr(worktreeDir.length + 1)
                Object.assign(src, { origin, scanned: posixify ? posixify(scannedRelpath) : scannedRelpath })
                if (!worktree) Object.assign(src, { realpath: src.abspath, abspath: src.scanned })
                filesInBucket.push(file)
              }
            }
          }
        }
      }
    }
    await fsp.rm(cacheDir, { recursive: true })
  })
}

async function prepareWorktree (repo) {
  const { dir, gitdir, ref, remote = 'origin', bare } = repo
  delete repo.remote
  const currentIndexPath = ospath.join(gitdir, 'index')
  const currentIndex = await fsp.readFile(currentIndexPath).catch(invariably.void)
  const worktreeGitdir = ospath.join(dir, '.git')
  const worktreeIndexPath = ospath.join(worktreeGitdir, 'index')
  const worktreeValidStatePath = ospath.join(worktreeGitdir, 'valid')
  try {
    let force = true
    try {
      await fsp.unlink(worktreeValidStatePath)
      await fsp.cp(worktreeIndexPath, currentIndexPath, { force })
      await removeUntrackedFiles(repo)
    } catch {
      force = false
      if (currentIndex) await fsp.unlink(currentIndexPath)
      await fsp.rm(dir, { recursive: true, force: true })
      await fsp.mkdir(worktreeGitdir, { recursive: true })
    }
    let head
    if (ref.startsWith('refs/heads/')) {
      head = `ref: ${ref}`
      const branchName = ref.substr(11)
      if (bare || !(await git.listBranches(repo)).includes(branchName)) {
        await git.branch({ ...repo, ref: branchName, object: `refs/remotes/${remote}/${branchName}`, force: true })
      }
    } else {
      head = await git.resolveRef(repo)
    }
    await git.checkout({ ...repo, force, noUpdateHead: true, track: false })
    await fsp.writeFile(ospath.join(worktreeGitdir, 'commondir'), `${gitdir}\n`, 'utf8')
    await fsp.writeFile(ospath.join(worktreeGitdir, 'HEAD'), `${head}\n`, 'utf8')
    await fsp.writeFile(worktreeValidStatePath, '')
    await fsp.cp(currentIndexPath, worktreeIndexPath)
  } finally {
    currentIndex ? await fsp.writeFile(currentIndexPath, currentIndex) : fsp.rm(currentIndexPath, { force: true })
  }
}

function generateWorktreeFolderName ({ url, gitdir, worktree }) {
  if (worktree === undefined) return ospath.basename(gitdir, '.git')
  return `${url.substr(url.lastIndexOf('/') + 1)}-${createHash('sha1').update(url).digest('hex')}`
}

function getBaseCacheDir ({ dir: dot, runtime: { cacheDir: preferredDir } }) {
  return preferredDir == null
    ? getUserCacheDir(`antora${process.env.NODE_ENV === 'test' ? '-test' : ''}`) || ospath.join(dot, '.cache/antora')
    : expandPath(preferredDir, { dot })
}

function srcFs (cwd, globs = '**/*', base) {
  return new Promise((resolve, reject, cache = Object.create(null), accum = [], relpathStart = cwd.length + 1) =>
    pipeline(
      globStream(globs, Object.assign({ cache, cwd }, GLOB_OPTS)),
      forEach(({ path: abspathPosix }, _, done) => {
        if (cache[abspathPosix].constructor === Array) return done() // detects some directories
        const abspath = ospath.sep === '\\' ? ospath.normalize(abspathPosix) : abspathPosix
        const relpath = base ? path.join('.', base, abspath.substr(relpathStart)) : abspath.substr(relpathStart)
        fsp.stat(abspath).then(
          (stat) => {
            if (stat.isDirectory()) return done() // detects directories that slipped through cache check
            fsp.readFile(abspath).then(
              (contents) => {
                const relpathPosix = posixify ? posixify(relpath) : relpath
                const basename = ospath.basename(relpathPosix)
                const extname = ospath.extname(relpathPosix)
                const stem = basename.slice(0, basename.length - extname.length)
                accum.push({
                  path: relpathPosix,
                  contents,
                  stat,
                  src: { path: relpathPosix, basename, stem, extname, abspath },
                })
                done()
              },
              (readErr) => done(readErr)
            )
          },
          (statErr) => done(statErr)
        )
      }),
      (err) => (err ? reject(err) : resolve(accum))
    )
  )
}

function removeUntrackedFiles (repo) {
  const trees = [git.STAGE({}), git.WORKDIR()]
  const map = (relpath, [sEntry]) => {
    if (relpath === '.') return
    if (relpath === '.git') return null
    if (sEntry == null) return fsp.rm(ospath.join(repo.dir, relpath), { recursive: true }).then(invariably.null)
    return sEntry.mode().then((mode) => (mode === 0o120000 ? null : undefined))
  }
  return git.walk({ ...repo, trees, map })
}
